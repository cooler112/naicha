ssh xiuxiulu.top
rm -rf /home/naicha
cd /home
git clone git@gitee.com:lisailiya/naicha.git
cd /home/naicha/naicha
mvn package
# 小程序-api
kill $(ps -ef|grep naicha_app |grep -v 'grep' | awk '{print $2}')
nohup java -jar -Xmx512m -Xmx512m -XX:+PrintGCDetails -Xloggc:/home/log/naicha_app/gc.log -Duser.timezone=GMT+08 naicha_app/target/naicha_app-1.0-SNAPSHOT.jar --spring.profiles.active=prod &
# 商家后台-api
kill $(ps -ef|grep naicha_system |grep -v 'grep' | awk '{print $2}')
nohup java -jar -Xmx512m -Xmx512m -XX:+PrintGCDetails -Xloggc:/home/log/naicha_system/gc.log -Duser.timezone=GMT+08 naicha_system/target/naicha_system-1.0-SNAPSHOT.jar --spring.profiles.active=prod &
# 浏览器访问 https://xxl.today/naicha-api/ping   http://47.98.206.60:8101/naicha-api/ping
# 浏览器访问 http://xxl.today/naicha-admin-api/ping http://47.98.206.60:9101/naicha-admin-api/ping

# 商家后台-ui
cd /home/naicha
rm -rf /usr/share/nginx/html/naicha_admin_ui
mkdir -p /usr/share/nginx/html/naicha_admin_ui
cp -r naicha_admin_ui/dist/* /usr/share/nginx/html/naicha_admin_ui/
kill $(cat /run/nginx.pid)
sleep 0.5
nginx
exit
# 浏览器访问 http://xxl.today/naicha_admin_ui

# h5端部署
sftp xxl.today
cd /usr/share/nginx/html
put -r D:/Desktop/naicha/naicha_ui/unpackage/dist/build/h5/ ./naicha_ui/
exit
ssh xxl.today
kill $(cat /run/nginx.pid)
sleep 0.5
nginx
exit
# 浏览器访问 https://xxl.today/naicha_ui
